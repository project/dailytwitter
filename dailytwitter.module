<?php
/**
 * @file
 * Code for Daily Twitter module.
 */

/**
 * Implements hook_help().
 */
function dailytwitter_help($section) {
  switch ($section) {
    case 'admin/help#dailytwitter':
      $output = '<p>The dailytwitter module allows users to cull together their twitter posts over the course of a day and display a add nicely formatted list of those posts to their own blog.  The functionality is effectively the same as LoudTwitter, except done from within your own Drupal site.</p>';
      return $output;
  }
}

/**
 * Implements hook_cron().
 */
function dailytwitter_cron() {
  $send_last = variable_get('dailytwitter_send_last', 0);
  $send_interval = variable_get('dailytwitter_send', 86400);
  $send_time = REQUEST_TIME -30;
  if (REQUEST_TIME - $send_last > $send_interval) {
    _dailytwitter_update();
    variable_set('dailytwitter_send_last', $send_time);
  }
}

/**
 * Implements hook_user_cancel().
 */
function dailytwitter_user_cancel($edit, $account, $method) {
  db_delete('dailytwitter')
  ->condition('uid', $user->uid)
  ->execute();
}

/**
 * Implements hook_user().
 */
function dailytwitter_user_OLD($type, &$edit, &$user, $category = NULL) { }

/**
 * Implements hook_permission().
 */
function dailytwitter_permission() {
  return array(
    'access dailytwitter' => array(
      'title' => t('access dailytwitter'),
      'description' => t('Can user make use of daily twitter?'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * @return array
 */
function dailytwitter_menu() {
  $items = array();

  $items['user/%user/dailytwitter'] = array(
    'title' => 'Daily Tweets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dailytwitter_user_settings_form', 1),
    'access callback' => 'user_access',
    'access arguments' => array('access dailytwitter'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Menu callback; show user notification options.
 */
function dailytwitter_user_settings_form($form, &$form_state, $arg) {
  global $user;
  if ($user->uid != $arg->uid && $user->uid != 1) {
    drupal_access_denied();
    return;
  }

  $account = user_load($arg->uid);
  if (!is_object($account)) {
    drupal_not_found();
    return;
  }

  $result = db_query('SELECT uid, screen_name FROM {dailytwitter} WHERE uid = :uid', array(':uid' => $account->uid));
  $dailytwitter = $result->fetchObject();
  $form = array();

  $form['dailytwitter_page_master']['screen_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Twitter username',
    '#default_value' => $dailytwitter->screen_name,
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => 'What is your twitter username?',
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

/**
 * Submits changes to user dailytwitter settings.
 */
function dailytwitter_user_settings_form_submit($form, &$form_state) {
  unset($form);

  db_delete('dailytwitter')
  ->condition('uid', $form_state['values']['uid'])
  ->execute();

  $id = db_insert('dailytwitter')
  ->fields(array(
    'uid' => $form_state['values']['uid'],
    'screen_name' => $form_state['values']['screen_name'],
  ))
  ->execute();

  drupal_set_message(t('Daily twitter settings saved.'));
}

function _dailytwitter_pull_yesterdays_posts($username) {
  $start = strtotime("yesterday midnight");
  $end = strtotime("today midnight");
  $datastr = file_get_contents("http://twitter.com/statuses/user_timeline/" . $username . ".xml?count=20");
  $xml = simplexml_load_string($datastr);

  $statuses = $xml->{'status'};
  $str = "";
  foreach ($statuses as $s) {
    $created_at = $s->{'created_at'};
    $created_time = strtotime($created_at);
    if ($created_time >= $end || $created_time < $start) {
      continue;
    }
    $time = strftime("%H:%M", $created_time);
    $text = $s->{'text'};
    $text = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]", "<a href=\"\\0\">\\0</a>", $text);
    $text = preg_replace('/\@(\w+)/', '@<a href="http://twitter.com/\\1">\\1</a>', $text);
    $str .= "<i>" . $time . "</i> " . $text . " <a href='http://twitter.com/" . $username . "/statuses/" . $s->{'id'} . "'>#</a>\n";
  }
  return $str;
}

function _dailytwitter_add_tweets_to_blog($uid, $username) {
  $tweets = _dailytwitter_pull_yesterdays_posts($username);
  if ($tweets != "") {
    // Create a new node
    $start = strtotime("yesterday midnight");
    $startdate = strftime("%m/%d/%Y", $start);

    $node = new stdClass();
    $node->type = 'blog';
    node_object_prepare($node);

    $node->title = 'Daily Twitter Posts - ' . $startdate;
    $node->language = LANGUAGE_NONE;
    $node->uid = $uid;

    $node->body[$node->language][0]['value'] = $tweets;
    $node->body[$node->language][0]['summary'] = text_summary($tweets);
    $node->body[$node->language][0]['format'] = 1;

    node_save($node);
  }
}

function _dailytwitter_update() {
  $sql = 'SELECT uid,screen_name FROM {dailytwitter}';
  $results = db_query($sql);
  foreach ($results as $row) {
    if ($row->screen_name != "") {
      _dailytwitter_add_tweets_to_blog($row->uid, $row->screen_name);
    }
  }
}
